#include "CrossModel.h"
#define 	VTK_SIZEOF_DOUBLE   KWIML_ABI_SIZEOF_DOUBLE

double CountDistance(const double* one, const double* two) {
    return std::sqrt((one[0] - two[0]) * (one[0] - two[0]) + (one[1] - two[1]) * (one[1] - two[1]) + (one[2] - two[2]) * (one[2] - two[2]));
}

namespace {

    vtkSmartPointer<vtkPolyData> ReadPolyData(const char* fileName)
    {
        vtkSmartPointer<vtkPolyData> polyData;
        std::string extension =
            vtksys::SystemTools::GetFilenameExtension(std::string(fileName));
        if (extension == ".ply")
        {
            vtkNew<vtkPLYReader> reader;
            reader->SetFileName(fileName);
            reader->Update();
            polyData = reader->GetOutput();
        }
        else if (extension == ".vtp")
        {
            vtkNew<vtkXMLPolyDataReader> reader;
            reader->SetFileName(fileName);
            reader->Update();
            polyData = reader->GetOutput();
        }
        else if (extension == ".obj")
        {
            vtkNew<vtkOBJReader> reader;
            reader->SetFileName(fileName);
            reader->Update();
            polyData = reader->GetOutput();
        }
        else if (extension == ".stl")
        {
            vtkNew<vtkSTLReader> reader;
            reader->SetFileName(fileName);
            reader->Update();
            polyData = reader->GetOutput();
        }
        else if (extension == ".vtk")
        {
            vtkNew<vtkPolyDataReader> reader;
            reader->SetFileName(fileName);
            reader->Update();
            polyData = reader->GetOutput();
        }
        else if (extension == ".g")
        {
            vtkNew<vtkBYUReader> reader;
            reader->SetGeometryFileName(fileName);
            reader->Update();
            polyData = reader->GetOutput();
        }
        else
        {
            vtkNew<vtkSphereSource> source;
            source->SetPhiResolution(25);
            source->SetThetaResolution(25);
            source->Update();
            polyData = source->GetOutput();
        }
        return polyData;
    }

} // namespace

CrossModel::CrossModel(int amount, std::string materials_filename) {
    this->model_parts_count = amount;
    std::ifstream infile(materials_filename);
    int material_code;
    int counter = 0;
    while (infile >>material_code)
    {
        materials_codes.push_back(material_code);
        ++counter;
    }
    if (counter < amount) {
        std::cout << "Materials properties file is not completed";
        return;
    }
    std::cout << "Successfully completed materials\n\r Please enter meshes filenames\n\r";

    meshes.resize(amount);
    obb_trees.resize(amount);
    std::string filename;
    for (int i = 0; i < amount; ++i) {
        std::cin >> filename;
        meshes[i] = ReadPolyData(filename.c_str());
        obb_trees[i] = vtkSmartPointer<vtkOBBTree>::New();
        obb_trees[i]->SetDataSet(meshes[i]);
        obb_trees[i]->BuildLocator();
        //red new mesh

        /*vtkSmartPointer<vtkPolyData> reader(filename);
        reader->SetFileName(filename);
        reader->Update();
        pds[i] = reader->GetOutput();*/

        vtkNew<vtkPolyDataConnectivityFilter> connectivityFilter;
        connectivityFilter->SetInputData(meshes[i]);
        connectivityFilter->Update();


        for (int j = 0; j < i; ++j) {
            vtkSmartPointer<vtkIntersectionPolyDataFilter> intersectionPolyDataFilter = vtkSmartPointer<vtkIntersectionPolyDataFilter>::New();

            vtkNew<vtkPolyDataConnectivityFilter> connectivityFilter_2;
            connectivityFilter_2->SetInputData(meshes[j]);
            connectivityFilter_2->Update();

            intersectionPolyDataFilter->SetInputConnection(0, connectivityFilter_2->GetOutputPort());
            intersectionPolyDataFilter->SetInputConnection(1, connectivityFilter->GetOutputPort());
            intersectionPolyDataFilter->Update();
            int am = intersectionPolyDataFilter->GetNumberOfIntersectionPoints();
            if (am > 0) {
                std::cout << "Input meshes has intersection, what is incorrect";
                return;
            }
        }
    }
    std::cout << "CrossModel creation completed!\n\r";
}
void CrossModel::CastRay() {
    vtkPoints* points = vtkPoints::New();
    vtkIdList* cellIds = vtkIdList::New();
    double pt1[3];
    double pt2[3];
    std::cout << "Please enter 6 numbers\n\r";
    std::cin >> pt1[0] >> pt1[1] >> pt1[2] >> pt2[0] >> pt2[1] >> pt2[2];
    const double p1[3] = { pt1[0], pt1[1], pt1[2] };
    const double p2[3] = { pt2[0], pt2[1], pt2[2] };
    for (int i = 0; i < obb_trees.size(); ++i) {
        auto code = obb_trees[i]->IntersectWithLine(p1, p2, points, cellIds);
        vtkDataArray* out_point = points->GetData();
        vtkIdType some_value = out_point->GetNumberOfTuples();
        double prev[3];
        for (int idx = 0; idx < some_value; ++idx) {
            if (idx % 2 == 0) {
                auto sss = out_point->GetTuple(idx);
                std::cout << "Intersection from point " << sss[0] << " " << sss[1] << " " << sss[2];
                memcpy(prev, sss, sizeof(double) * 3);
            }
            else {
                auto ss = out_point->GetTuple(idx);
                std::cout << " to " << ss[0] << " " << ss[1] << " " << ss[2] << " for the length " << CountDistance(prev, ss) << " with part, which material is " << materials_codes[i] << "\n\r";
            }
        }
        //double dist = CountDistance(in_point, out_point);
    }
}

void CrossModel::CreateRayMatrix(int x, int y, double start_coordinate[3], std::vector<double>& points) {
    if (x * y * 3 != points.size()) {
        std::cout << "Amount of points coordinates not match input x and y";
        return;
    }
    static_matrix.resize(x, std::vector<double*>(y));
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < y; j+=3) {
            this->static_matrix[x][y][0] = points[j];
            this->static_matrix[x][y][1] = points[j+1];
            this->static_matrix[x][y][2] = points[j+2];
        }
    }
    this->matrix_ray_start_point[0] = start_coordinate[0];
    this->matrix_ray_start_point[1] = start_coordinate[1];
    this->matrix_ray_start_point[2] = start_coordinate[2];
}

void CrossModel::CastRaySeries(int x, int y) {
    double pt1[3];
    double pt2[3];
    if (x == -1 && y == -1) {
        std::cout << "Let's cast ray series! Please enter coordinates of the start ray points:\n\r";
        std::cin >> pt1[0] >> pt1[1] >> pt1[2] >> pt2[0] >> pt2[1] >> pt2[2];
    }
    else {
        pt1[3] = *matrix_ray_start_point;
        pt2[3] = *static_matrix[x][y];
    }

    const double p1[3] = { pt1[0], pt1[1], pt1[2] };
    const double p2[3] = { pt2[0], pt2[1], pt2[2] };
    double previous_start_point[3] = { pt1[0], pt1[1], pt1[2] };
    double previous_end_point[3] = { pt2[0], pt2[1], pt2[2] };

    int cast_count = int(360 / 2);      //rotation angle amount
    double cos = 0.99939083;
    double sin = 0.0348995;


    const int cross_array_size = cast_count * model_parts_count;

    std::string output_filename = "matrix_ray_casting_result.txt";
    std::ofstream outwrite(output_filename);


    for (int i = 0; i < cast_count; ++i) {
        const double point_1[3] = { previous_start_point[0], previous_start_point[1], previous_start_point[2] };
        const double point_2[3] = { previous_end_point[0], previous_end_point[1], previous_end_point[2] };
        for (int j = 0; j < obb_trees.size(); ++j) {
            const double point_1[3] = { previous_start_point[0], previous_start_point[1], previous_start_point[2] };
            const double point_2[3] = { previous_end_point[0], previous_end_point[1], previous_end_point[2] };
            vtkPoints* points = vtkPoints::New();
            auto code = obb_trees[j]->IntersectWithLine(point_1, point_2, points, nullptr);
            vtkDataArray* out_point = points->GetData();
            vtkIdType some_value = out_point->GetNumberOfTuples();
            auto data = out_point->GetRange();
            double prev[3];
            for (int idx = 0; idx < some_value; ++idx) {
                if (idx % 2 == 0) {
                    auto sss = out_point->GetTuple(idx);
                    outwrite << "from " << sss[0] << " " << sss[1] << " " << sss[2];
                    std::cout << "Intersection from point " << sss[0] << " " << sss[1] << " " << sss[2];
                    memcpy(prev, sss, sizeof(double) * 3);
                }
                else {
                    auto ss = out_point->GetTuple(idx);
                    outwrite << " to " << ss[0] << " " << ss[1] << " " << ss[2] << " with distance " << CountDistance(prev, ss) << materials_codes[j] << "\n\r";
                    std::cout << " to " << ss[0] << " " << ss[1] << " " << ss[2] << " for the length " << CountDistance(prev, ss) << " with part, which material is " << materials_codes[j] << "\n\r";
                }
            }
        }

        double x1 = point_1[0];
        double x2 = point_2[0];
        double z2 = point_2[2];
        double z1 = point_1[2];

        previous_start_point[0] = x1 * cos + z1 * sin;
        previous_start_point[2] = z1 * cos - x1 * sin;
        previous_end_point[0] = x2 * cos + z2 * sin;
        previous_end_point[2] = z2 * cos - x2 * sin;
    }

}
