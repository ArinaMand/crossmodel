#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>

#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkOBBTree.h>
#include <vtkIntersectionPolyDataFilter.h>
#include <vtkSTLReader.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataConnectivityFilter.h>
//readers
#include <vtkBYUReader.h>
#include <vtkOBJReader.h>
#include <vtkPLYReader.h>
#include <vtkPolyDataReader.h>
#include <vtkSTLReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkSphereSource.h>


class CrossModel {
private:
    int model_parts_count;
    std::vector<int> materials_codes;
    std::vector<vtkSmartPointer<vtkPolyData>> meshes;
    std::vector<vtkSmartPointer<vtkOBBTree>> obb_trees;
    std::vector<vtkSmartPointer<vtkPolyDataConnectivityFilter>> intersection_polygones;
    std::vector<std::vector<double*>> static_matrix;
    double matrix_ray_start_point[3];

public:
    CrossModel(int amount, std::string materials_filename);
    void CastRay();
    void CastRaySeries(int x, int y);
    void CreateRayMatrix(int x, int y, double start_coordinate[3], std::vector<double>& points);
};
namespace {

    vtkSmartPointer<vtkPolyData> ReadPolyData(const char* fileName);

}
