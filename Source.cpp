#include <iostream>
#include "CrossModel.h"
#include <chrono>

int main() {
	//csm.CastRay();
	std::chrono::system_clock seconds;
	std::chrono::time_point<std::chrono::system_clock> m_StartTime;
	std::chrono::time_point<std::chrono::system_clock> m_EndTime;
	//std::string files[6] = { "pyramid.stl", "Ball.stl", "Soapbox-left.stl", "Snowflake.stl", "moose.stl", "Volcano.stl"};
	for (int i = 0; i < 6; ++i) {
		int cr_time[10];
		int rtime[10];
		for (int j = 0; j < 10; ++j) {
			m_StartTime = std::chrono::system_clock::now();
			CrossModel csm(1, "materials.txt");
			m_EndTime = std::chrono::system_clock::now();
			cr_time[j] = std::chrono::duration_cast<std::chrono::milliseconds>(m_EndTime - m_StartTime).count();
			m_StartTime = std::chrono::system_clock::now();
			//csm.CastRay();
			csm.CastRaySeries(-1, -1);
			m_EndTime = std::chrono::system_clock::now();
			rtime[j] = std::chrono::duration_cast<std::chrono::milliseconds>(m_EndTime - m_StartTime).count();
		}
		//std::cout << "\'"<< files[i] << "\' : [ ";
		for (int j = 0; j < 10; ++j) {
			std::cout << cr_time[j] << ", ";
		}
		for (int j = 0; j < 10; ++j) {
			std::cout << rtime[j] << ", ";
		}
		std::cout << "]\n\r";
	}
}